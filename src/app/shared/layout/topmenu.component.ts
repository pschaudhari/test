import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-layout-topmenu',
  templateUrl: './topmenu.component.html'
})
export class TopmenuComponent implements OnInit {
  title = '';
  constructor(private titleService: Title) { }

  ngOnInit() {
    const appTitle = this.titleService.getTitle();
    this.title = this.titleService.getTitle();
  }
}
