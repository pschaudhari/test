import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanybillComponent } from './companybill.component';

describe('CompanybillComponent', () => {
  let component: CompanybillComponent;
  let fixture: ComponentFixture<CompanybillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanybillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanybillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
