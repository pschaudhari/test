import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { CompanyComponent } from './company/company.component';
import { CompanybillComponent } from './companybill/companybill.component';
import { EmployeeComponent } from './employee/employee.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component:  LoginComponent },
  { path: 'company', component: CompanyComponent },
  { path: 'companybill', component: CompanybillComponent},
  { path: 'employee', component: EmployeeComponent },
  { path: 'payment', component: PaymentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
